package singleton;

public class App {

    public static void main(String[] args) {
        JsonParser instance = JsonParser.getInstance();
        System.out.println("Valor inicial: " + instance.getValor());
        instance.setValor("Pato");
        System.out.println("Valor atual: " + instance.getValor());
        System.out.println();

        System.out.println(instance.converterParaJson(gerarUsuario()));

        System.out.println(JsonParser.getInstance().converterParaJson(gerarUsuario()));

        System.out.println();
        System.out.println("É o mesmo objeto? " + (instance == JsonParser.getInstance()));

        // não é possível instanciar a classe
        // o único método acessível é o getInstance()
        // new JsonParser();

        // como está o valor agora?
        System.out.println("Valor atual: " + JsonParser.getInstance().getValor());
    }

    private static Usuario gerarUsuario() {
        return Usuario.builder()
                .id(123L)
                .nome("Ze")
                .sobrenome("Migué")
                .cpf("03422345600")
                .rg("8277446487")
                .idade(25)
                .telefone("5551987625447")
                .endereco("Rua dos patos")
                .nacionalidade("Brasil")
                .build();
    }

}