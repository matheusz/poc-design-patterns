package singleton;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Usuario {

    private Long id;
    private String nome;
    private String sobrenome;
    private String cpf;
    private String rg;
    private Integer idade;
    private String telefone;
    private String endereco;
    private String nacionalidade;

}
