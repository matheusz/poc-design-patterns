package singleton;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParser {

    private static JsonParser instance = new JsonParser();
    private ObjectMapper objectMapper;
    private String valor;

    private JsonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public static JsonParser getInstance() {
        return instance;
    }

    public String converterParaJson(Object object) {
        try {
            return this.objectMapper.writeValueAsString(object);
        } catch (Exception error) {
            return null;
        }
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
